/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 50);
/******/ })
/************************************************************************/
/******/ ({

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(51);


/***/ }),

/***/ 51:
/***/ (function(module, exports) {

$(document).ready(function () {
    var cart = [];
    var total = 0.0;
    var dt = $("#products-table").DataTable({
        columnDefs: [{
            targets: [0],
            visible: false
        }]
    });
    $(".delete-product").on('click', function (e) {
        var id = $(this).data('id');
        swal({
            title: 'Confirmación de acción',
            text: "¿Estás seguro de eliminar este producto?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
        }).then(function (result) {
            if (result.value) {
                $("#form-delete").attr('action', site_url + "/products/" + id).submit();
            }
        }).catch(function (err) {});
    });
    $(".add-to-cart").on('click', function () {
        var self = this;
        var row = dt.row($(this).parent().parent());
        var cell = dt.cell($(this).parent());
        var data = row.data();
        if (data[6] > 0) {
            var exists = null;
            cart.map(function (item, index) {
                if (item.product == data[0]) {
                    exists = index;
                }
            });
            console.log("exists ", exists);
            if (exists !== null) {
                cart[exists].quantity++;
            } else {
                cart.push({
                    image: data[1],
                    product: data[0],
                    quantity: 1,
                    name: data[2],
                    price: data[4]
                });
            }
            data[6]--;
        } else {
            M.toast({ html: 'Cantidad insuficiente' });
        }

        $("#cart").empty();
        $("#cart").append('<ul class="list-group">');
        totales = [];
        items = cart.map(function (product) {
            totales.push(product.quantity * product.price);
            return "\n                <li class=\"collection-item avatar\" id=\"" + product.product + "\">\n                    " + product.image + "\n                    <span class=\"title\">Nombre del producto " + product.name + "</span>\n                    <p>Precio: " + product.price + "\n                    </p>\n                    <p>\n                        Cantidad: " + product.quantity + "\n                    </p>\n                    <p>\n                        Total: " + (product.quantity * product.price).toFixed(2) + "\n                    </p>\n                    <a href=\"#!\" data-priceitem=\"" + product.quantity * product.price + "\" data-product=\"" + product.product + "\" class=\"remove_from_cart secondary-content\"><i class=\"fa fa-times\"></i></a>\n                </li>\n            ";
        });
        total = totales.reduce(function (sum, item) {
            return sum + item;
        });
        $("#cart").append(items);
        $("#cart").append("<li class=\"collection-item\"> Total: <span id=\"total_price\">" + total.toFixed(2) + "</span> </li>");
        $("#cart").append('</ul>');
        if (cart.length > 0) {
            $("#show_cart").show();
        } else {
            $("#show_cart").hide();
        }
    });
    $("#cart").on('click', '.remove_from_cart', function (e) {
        e.preventDefault();
        var item = $(this);
        var product = item.data('product');
        var priceitem = item.data('priceitem');
        new_cart = cart.filter(function (item2) {
            console.log([parseInt(item2.product, 10), parseInt(product, 10)]);
            return parseInt(item2.product, 10) != parseInt(product, 10);
        });
        cart = new_cart;
        console.log("product to remove: ", product, ' cart now: ', new_cart.length);
        total = (total - priceitem).toFixed(2);
        $("#total_price").text(total);
        $("#" + product).remove();
        if (cart.length < 1) {
            console.log("hide the modal");
            $("#show_cart").hide();
            $("#modal1").modal().close();
        }
        console.log(cart);
    });
    $("#create_sale").click(function () {
        M.toast({ html: 'Aún no me sale prro :(' });
    });
});

/***/ })

/******/ });