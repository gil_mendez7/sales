<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use softDeletes;
    protected $fillable = [
        'name', 'description','active','price_in_cents','image','stock', 'type_id'
    ];
    public function setPriceInCentsAttribute($value){
        $this->attributes['price_in_cents'] = $value * 100; 
    }
    public function getPriceInCentsAttribute($price){
        return $price / 100;
    }
    public function getImageAttribute($image){
        return Storage::url($image);
    }
    public function type(){
        return $this->belongsTo(Type::Class);
    }
    public function categories(){
        return $this->belongsToMany(Category::class);
    }
}
