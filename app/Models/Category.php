<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use softDeletes;
    protected $table = 'categories';
    protected $fillable = [
        'name', 'active'
    ];
    public function products(){
        return $this->belongsToMany('products');
    }
}
