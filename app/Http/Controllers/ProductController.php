<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Type;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    protected $model;
    protected $categories;
    protected $types;
    public function __construct(Product $product, Category $category, Type $type){
        $this->model = new Repository($product);
        $this->categories = new Repository($category);
        $this->types = new Repository($type);
        $this->middleware('onlyAdmin')->only(['store','update','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['products'] = $this->model->with('categories')->get();
        $data['categories'] = $this->categories->all()->pluck('name','id');
        $data['types'] = $this->types->all();
        return view('products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_path = '/images/products/no-product.png';
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('public/images/products');
        }
        $product = new Product([
            'name' => $request->name,
            'description' => $request->description,
            'stock' => $request->stock,
            'price_in_cents' => $request->price_in_cents, 
            'type_id' => $request->type_id,
            'image' => $image_path
        ]);
        $product->save();
        $product->categories()->attach($request->categories);
        
        Session::flash('message','Producto creado con éxito!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['product'] = $this->model->with(['categories','type'])->find($id);
        $data['categories'] = $this->categories->all()->pluck('name','id');
        $data['types'] = $this->types->all()->pluck('name','id');
        return view('products.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        
        if($request->hasFile('image')){
            Storage::delete($product->image);
            $image_path = $request->file('image')->store('public/images/products');
            $product->update(['image' => $image_path]);
        }
        $product->update($request->only('active','name','description','stock','price_in_cents','type_id'));
        $product->categories()->detach();
        $product->categories()->attach($request->categories); 
        Session::flash('message','Producto actualizado con éxito!');
        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try{
            $product->delete();
            Session::flash('message', 'Producto eliminado con éxito!');
            return redirect('/products');
        }
        catch(Exception $e){
            Session::flash('message','Error eliminando el producto.');
            Session::flash('alert-class','danger');
            return redirect()->back();
        }
    }
}
