<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Type;
use Illuminate\Http\Request;
use App\Repositories\Repository;

class TypeController extends Controller
{
    protected $types;
    public function __construct(Type $type){
        $this->types = new Repository($type);
        $this->middleware('onlyAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['types'] = $this->types->all();
        return view('types.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->types->create($request->only('name'));
        Session::flash('message','Tipo de producto creado con éxito!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $type->update($request->only('name','active'));
        Session::flash('message','Tipo de producto actualizado con éxito!');
        return redirect('/types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->types->delete($id);
            Session::flash('message','Tipo de producto eliminado de manera correcta');
            return redirect('/types');
        }
        catch(Exception $e){
            Session::flash('message','Error al eliminar el tipo de producto!');
            Session::flash('alert-class','danger');
            return redirect('/types');
        }
    }
}
