<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\User::create([
            'name' => 'Administrador',
            'username' => 'admin',
            'lastname' => ' Sistema',
            'password' => bcrypt('administrador'),
            'email' => 'admin@mail.com',
            'admin' => true,
        ]);
        $normal = \App\User::create([
            'name' => 'Empleado',
            'username' => 'empleado',
            'lastname' => ' Sistema',
            'password' => bcrypt('empleado'),
            'email' => 'empleado@mail.com',
            'admin' => false,
        ]);
    }
}
