
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.swal = require('sweetalert2')
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
$(document).ready(function () {
    $(".select2").select2();
    $('.sidenav').sidenav();
    var modal = $('.modal').modal();
    $(".close-modal").click(function(e){
        e.preventDefault();
        var modal_to_close = $(this).data('modal')
        var instance = $(`#${modal_to_close}`).modal()
        try_to_close = M.Modal.getInstance(instance);
        try_to_close.close()
    })
});
