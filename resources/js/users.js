$(document).ready(function(){
    var dt = $("#users-table").DataTable({
        columnDefs: [
            {
                targets: [5],
                visible: false
            }
        ]
    })
    $(".delete-user").on('click', function(e){
        var id = $(this).data('id')
        swal({
            title: 'Confirmación de acción',
            text: "¿Estás seguro de eliminar a este usuario?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
            })
        .then((result) => {
            console.log(result)
            if (result.value) {
                $("#form-delete").attr('action',`${site_url}/users/${id}`).submit()
            }
        })
        .catch(err => {

        })
    })
    $(".edit-user").on('click', function(e){
        var id = $(this).data('id')
        $("#edit-form").attr('action', `${site_url}/users/${id}`)
        var data = dt.row($(this).parent().parent()).data()
        console.log(data)
        $("#name_edit").val(data[0])
        $("#lastname_edit").val(data[1])
        $("#email_edit").val(data[2])
        $("#admin_edit").val(data[5]).trigger('change')
        
    })
})