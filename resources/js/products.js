$(document).ready(function(){
    var cart = []
    var total = 0.0
    var dt = $("#products-table").DataTable({
        columnDefs: [
            {
                targets: [0],
                visible: false
            }
        ]
    })
    $(".delete-product").on('click', function(e){
        var id = $(this).data('id')
        swal({
            title: 'Confirmación de acción',
            text: "¿Estás seguro de eliminar este producto?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText:'Cancelar'
            })
        .then((result) => {
            if (result.value) {
                $("#form-delete").attr('action',`${site_url}/products/${id}`).submit()
            }
        })
        .catch(err => {
            
        })
    })
    $(".add-to-cart").on('click', function(){
        var self = this
        var row = dt.row($(this).parent().parent())
        var cell = dt.cell($(this).parent())
        var data = row.data()
        if(data[6] > 0){
            var exists = null
            cart.map((item, index) => {
                if(item.product == data[0]){
                    exists =  index
                }
            })
            console.log("exists ",exists)
            if(exists !== null){
                cart[exists].quantity++
            }
            else {
                cart.push(
                    {
                        image: data[1],
                        product: data[0],
                        quantity: 1,
                        name: data[2],
                        price : data[4]
                    }
                )
            }
            data[6]--   
            
        }
        else {
            M.toast({ html: 'Cantidad insuficiente' })

        }
        
        $("#cart").empty()
        $("#cart").append('<ul class="list-group">')
        totales = []
        items = cart.map(product => {
            totales.push( product.quantity * product.price )
            return `
                <li class="collection-item avatar" id="${product.product}">
                    ${product.image}
                    <span class="title">Nombre del producto ${product.name}</span>
                    <p>Precio: ${product.price}
                    </p>
                    <p>
                        Cantidad: ${product.quantity}
                    </p>
                    <p>
                        Total: ${(product.quantity * product.price).toFixed(2)}
                    </p>
                    <a href="#!" data-priceitem="${product.quantity * product.price}" data-product="${product.product}" class="remove_from_cart secondary-content"><i class="fa fa-times"></i></a>
                </li>
            `
        })
        total = totales.reduce( (sum, item) => sum + item)
        $("#cart").append(items)
        $("#cart").append(`<li class="collection-item"> Total: <span id="total_price">${total.toFixed(2)}</span> </li>`)
        $("#cart").append('</ul>')
        if(cart.length > 0 ){
            $("#show_cart").show();
        }
        else {
            $("#show_cart").hide();
        }
    })
    $("#cart").on('click', '.remove_from_cart', function(e){
        e.preventDefault()
        var item = $(this)
        var product = item.data('product')
        var priceitem = item.data('priceitem')       
        new_cart = cart.filter( item2  => {
            console.log([parseInt(item2.product,10), parseInt(product,10)])
            return parseInt(item2.product, 10) != parseInt(product, 10)
        })
        cart = new_cart
        console.log("product to remove: ", product, ' cart now: ', new_cart.length)
        total = (total - priceitem).toFixed(2)
        $("#total_price").text(total)
        $(`#${product}`).remove()
        if(cart.length < 1){
            console.log("hide the modal")
            $("#show_cart").hide();
            $("#modal1").modal().close()
        }
        console.log(cart)
    })
    $("#create_sale").click(function(){
        M.toast({ html: 'Aún no me sale prro :(' })

    })
})