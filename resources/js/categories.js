$(document).ready(function(){
    var dt = $("#categories-table").DataTable({
        columnDefs: [
            {
                targets: [3],
                visible: false
            }
        ]
    })
    $(".delete-category").on('click', function(e){
        var id = $(this).data('id')
        swal({
            title: 'Confirmación de acción',
            text: "¿Estás seguro de eliminar este tipo de producto?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
            })
        .then((result) => {
            console.log(result)
            if (result.value) {
                $("#form-delete").attr('action',`${site_url}/categories/${id}`).submit()
            }
        })
        .catch(err => {

        })
    })
    $(".edit-category").on('click', function(e){
        var id = $(this).data('id')
        $("#edit-form").attr('action', `${site_url}/categories/${id}`)
        var data = dt.row($(this).parent().parent()).data()
        $("#name_edit").val(data[0])
        $("#active_edit").val(data[3]).trigger('change')
        
    })
    
})