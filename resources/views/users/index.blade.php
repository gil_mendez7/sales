@extends('layouts.master')
@section('content')
    <h4><i class="fa fa-users"></i> Usuarios</h4>
    <button class="btn modal-trigger" data-target="modal">
        <i class="fa fa-plus"></i>
        Agregar
    </button>
    <table id="users-table" class="table table-striped table-hovered">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Apellidos
                </th>
                <td>
                    E-mail
                </td>
                <td>
                    Tipo de usuario
                </td>
                <th>
                    Acciones
                </th>
                <th>

                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>
                        {{$user->name}}
                    </td>
                    <td>
                        {{$user->lastname}}
                    </td>
                    <td>
                        {{$user->email}}
                    </td>
                    <td>
                        @if ($user->admin)
                            <span class="badge  white-text green">Administrador</span>
                        @else
                            <span class="badge  white-text orange">Normal</span>
                        @endif
                    </td>
                    <td>
                        <button data-id="{{$user->id}}" class="modal-trigger btn btn-info btn-sm edit-user" data-toggle="modal" data-target="modalEdit">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button data-id="{{$user->id}}" class="btn btn-sm btn-danger delete-user">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                    <td>
                        {{$user->admin}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @component('components.modal')
        @slot('title')
            Crear usuario
        @endslot
        @slot('content')
            {!! Form::open(['url' => 'users', 'method' => 'post','files' => true]) !!}
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input id="name" name="name" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="lastname">Apellidos</label>
                    <input id="lastname" name="lastname" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input id="email" name="email" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Contraseña para el usuario</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="admin">Tipo de usuario</label>
                    <select name="admin" id="admin" class="select2">
                        <option value="0">Normal</option>
                        <option value="1">Administrador</option>
                    </select>
                </div>
            
        @endslot
        @slot('button_id')
            btn_add_user
        @endslot
        @slot('icon')
            <i class="fa fa-plus"></i>
        @endslot
        @slot('title_action')
            Guardar
            {!! Form::close() !!}
        @endslot
    @endcomponent
    @component('components.modal')
        @slot('title')
            <i class="fa fa-user"></i>
            Editar usuario
        @endslot
        @slot('nombremodal','modalEdit')
        @slot('content')
            {!! Form::open(['url' => 'users', 'method' => 'put', 'id' => 'edit-form']) !!}
                <div class="form-group">
                    <label for="name_edit">Nombre</label>
                    <input id="name_edit" name="name" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="lastname_edit">Apellidos</label>
                    <input id="lastname_edit" name="lastname" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="email_edit">E-mail</label>
                    <input id="email_edit" name="email" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="admin_edit">Tipo de usuario</label>
                    <select name="admin" id="admin_edit" class="select2">
                        <option value="0">Normal</option>
                        <option value="1">Administrador</option>
                    </select>
                </div>
        @endslot
        @slot('button_id')
            btn_update_type
        @endslot
        @slot('icon')
            <i class="fa fa-refresh"></i>
        @endslot
        @slot('title_action')
            Actualizar
            {!! Form::close() !!}
        @endslot
    @endcomponent
    {!! Form::open(['url' => '/users', 'method' =>'delete', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}
@endsection
@section('extra-js')    
    <script src="{{asset('js/users.js')}}"></script>
@endsection