@php
    $user = Auth::user();
@endphp
@extends('layouts.master')

@section('content')
    <h4><i class="fa fa-clipboard"></i> Productos</h4>
    @if ($user->admin)
        <button data-target="modal" class="modal-trigger btn">
            <i class="fa fa-plus"></i>
            Agregar
        </button>
        
    @endif
    <div class="row">
        <div class="col s12">
                <table id="products-table" class="table table-striped table-hovered">
                    <thead>
                        <tr>
                            <th>
                                Id
                            </th>
                            <th>
                                Imagen
                            </th>
                            <th>
                                Nombre
                            </th>
                            <td>
                                Descripción
                            </td>
                            <td>
                                Precio
                            </td>
                            <td>
                                Tipo
                            </td>
                            <td>
                                Stock
                            </td>
                            <td>
                                Status
                            </td>
                            <td>
                                Categorias
                            </td>
                            
                            <th>
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>
                                    {{$product->id}}
                                </td>
                                <td>
                                    <img width="50px;" src="{{$product->image}}" alt="{{$product->title}}">
                                </td>
                                <td>
                                    {{$product->name}}
                                </td>
                                <td>
                                    {{$product->description}}
                                </td>
                                <td>
                                    {{$product->price_in_cents}}
                                </td>
                                <td>
                                    {{$product->type ? $product->type->name : ''}}
                                </td>
                                <td>
                                    {{$product->stock}}
                                </td>
                                <td>
                                    @if ($product->active)
                                        <span class="badge new green">Activo</span>
                                    @else
                                        <span class="badge new red">Inactivo</span>
                                    @endif
                                    
                                </td>
                                <td>
                                    @foreach ($product->categories as $category)
                                        <span class="badge blue white-text new">{{$category->name}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @if ($user->admin)
                                        <a href="{{url("/products/$product->id")}}" class="btn btn-small">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button data-id="{{$product->id}}" class="btn btn-small delete-product">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    @else 
                                        <button class="add-to-cart btn btn-small">
                                            <i class="fa fa-cart-plus"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
    
    @component('components.modal')
        @slot('title')
            Crear producto
        @endslot
        @slot('content')
            {!! Form::open(['url' => 'products', 'method' => 'post','files' => true]) !!}
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input id="name" name="name" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <input id="description" name="description" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="stock">Stock</label>
                    <input id="stock" name="stock" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="price_in_cents">Precio</label>
                    <input id="price_in_cents" name="price_in_cents" type="number" step="0.01" value="0.0" min="0.0" class="form-control">
                </div>  
                <div class="form-group">
                    <label for="image">Imagen</label>
                    <input id="image" name="image" type="file" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="type_id">Tipo</label>
                    <select class="form-control" name="type_id" id="type_id">
                        @foreach ($types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories[]">Categorias</label>
                    {!!  Form::select('categories[]', $categories, null, ['id' => 'categories','class' => 'select2 form-control' , 'multiple' => 'multiple']) !!}
                </div> 
            
        @endslot
        @slot('button_id')
            btn_add_product
        @endslot
        @slot('icon')
            <i class="fa fa-plus"></i>
        @endslot
        @slot('title_action')
            Guardar
            {!! Form::close() !!}
        @endslot
    @endcomponent
    {!! Form::open(['url' => '/products', 'method' =>'delete', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large modal-trigger" id="show_cart" style="display: none" href="#modal1">
            <i class="fa fa-shopping-cart "></i>
        </a>
    </div>
    
    <div id="modal1" class="modal bottom-sheet">
        <div class="modal-content">
            <h4>Productos en el carrito</h4>
            <ul class="collection" id="cart">
                
            </ul>
        </div>
        
        <div class="modal-footer">
            <button class="btn red close-modal" data-modal="modal1">
                Cancelar
                <i class="fa fa-times"></i>
            </button>
            <a href="#!" id="create_sale" class="modal-close waves-effect green white-text waves-green btn-flat">
                Registrar compra
                <i class="fa fa-check"></i>
            </a>
        </div>
    </div>
@endsection
@section('extra-js')    
    <script src="{{asset('/js/products.js')}}"></script>
@endsection