@extends('layouts.master')

@section('content')
    <h3>Editar producto</h3>
    <div class="text-center">
        <img src="{{$product->image}}" width="50%" class="img-fluid" alt="{{$product->title}}">
    </div>
    
    {!! Form::open(['url' => "/products/$product->id", 'method' => 'PUT','files' => true]) !!}
        <div class="row">            
            <div class="input-field col s12">
                <label for="name">Nombre</label>
                <input id="name" name="name" type="text" value="{{$product->name}}" class="">
            </div> 
            <div class="input-field col s12">
                <label for="description">Descripción</label>
                <input id="description" name="description" value="{{$product->description}}" type="text" class="">
            </div> 
            <div class="input-field col s12">
                <label for="stock">Stock</label>
                <input id="stock" name="stock" type="text" value="{{$product->stock}}" class="">
            </div>
            <div class="input-field col s12">
                <label for="price_in_cents">Precio</label>
                <input id="price_in_cents" name="price_in_cents" value="{{$product->price_in_cents}}" type="number" step="0.01" min="0.0" class="">
            </div>  
            <div class="input-field col s12">
                <label for="image">Imagen</label>
                <input id="image" name="image" type="file" class="">
            </div> 
            <div class="input-field col s12">
                <label for="type_id">Tipo</label>
                {!! Form::select('type_id', $types, $product->type_id, ['class' => 'select2', 'id' => 'type_id']) !!}
            </div>
            <div class="input-field col s12">
                
                <label for="categories[]">Categorias</label>
                {!!  Form::select('categories[]', $categories, $product->categories->pluck('id'), ['id' => 'categories','class' => 'select2 ' , 'multiple' => 'multiple']) !!}
            </div> 
            <div class="input-field col s12">
                <label for="active">Estatus:</label>
                {!! Form::select('active', [0 => 'No activo', 1 => 'Activo'], $product->active, ['class' => 'select2']) !!}
            </div>
            @component('components.buttons')
                @slot('url')
                    {{url('/products')}}
                @endslot
                @slot('body_button')
                    <i class="fa fa-refresh"></i>
                    Actualizar
                @endslot
            @endcomponent
        </div>
    {!! Form::close() !!}

@endsection