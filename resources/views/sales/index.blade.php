@extends('layouts.master')

@section('content')
    <h4><i class="fa fa-shopping-cart"></i> Ventas</h4>
    <table id="categories-table" class="table table-striped table-hovered">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Usuario
                </th>
                <th>
                    Fecha de venta
                </th>
                <th>
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sales as $sale)
                <tr>
                    <td>
                        {{$sale->id}}
                    </td>
                    <td>
                        {{$sale->user ? $sale->user->name : ''}}
                    </td>
                    <td>
                        {{$sale->created_at}}
                    </td>
                    <td>
                        <button>
                            <i clas="fa fa-eye"></i>
                            Ver detalle
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@section('extra-js')    
    <script src="{{asset('js/sales.js')}}"></script>
@endsection