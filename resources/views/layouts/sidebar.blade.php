@php
    $user = Auth::user();
@endphp
<ul id="slide-out" class="sidenav sidenav-fixed  teal">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="{{asset('images/users/background-user.jpg')}}">
            </div>
            <a href="#user"><img width="50px" class="circle" src="{{asset('images/users/user.png')}}"></a>
            <a href="#name"><span class="name white-text">
                {{$user->name}}
                </span>
            </a>
            <a href="#email"><span class="email white-text">{{$user->email}}</span></a>
        </div>
    </li>
    <li>
        <a href="{{url('/products')}}" class="sidenav-close white-text">
            <i class="fa fa-clipboard"></i>
            Productos
        </a>
        
        
    </li>
    @if ($user->admin)
        <li>
            <a href="{{url('/types')}}" class="sidenav-close white-text">
                <i class="fa fa-bookmark"></i>
                
                Tipo de productos
            </a>
        </li>
        <li>
            <a href="{{url('/categories')}}" class="sidenav-close white-text">
                <i class="fa fa-tags"></i>
                Categorias de productos
            </a>
        </li>
        <li>
            <a href="{{url('/sales')}}" class="sidenav-close white-text">
                <i class="fa fa-shopping-cart"></i>
                Ventas
            </a>
        </li>
        <li>
            <a href="{{url('/users')}}" class="sidenav-close white-text">
                <i class="fa fa-users"></i>
                Usuarios
            </a>
        </li>
    @endif
    <li>
        <a class="white-text" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off"></i> 
            Cerrar sesión
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </li>
</ul>
  

