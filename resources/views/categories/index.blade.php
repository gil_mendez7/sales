@extends('layouts.master')

@section('content')
    <h4><i class="fa fa-tag"></i> Categorias</h4>
    <button class="btn modal-trigger" data-target="modal">
        <i class="fa fa-plus"></i>
        Agregar
    </button>
    <table id="categories-table" class="table table-striped  table-hover">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Estatus
                </th>
                <th>
                    Acciones
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>
                        {{$category->name}}
                    </td>
                    <td>
                        @if ($category->active)
                            <span class="badge green white-text">Activo</span>
                        @else
                            <span class="badge red white-text">Inactivo</span>
                        @endif
                    </td>
                    <td>
                        <button data-id="{{$category->id}}" class="modal-trigger btn btn-info btn-sm edit-category" data-toggle="modal" data-target="modalEdit">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button data-id="{{$category->id}}" class="btn btn-sm btn-danger delete-category">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                    <td>
                        {{$category->active}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @component('components.modal')
        @slot('title')
            Crear categoria
        @endslot
        @slot('content')
            {!! Form::open(['url' => 'categories', 'method' => 'post']) !!}
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input id="name" name="name" type="text" class="form-control">
                </div> 
            
        @endslot
        @slot('button_id')
            btn_add_category
        @endslot
        @slot('icon')
            <i class="fa fa-plus"></i>
        @endslot
        @slot('title_action')
            Guardar
            {!! Form::close() !!}
        @endslot
    @endcomponent
    @component('components.modal')
        @slot('title')
            Editar categoria
        @endslot
        @slot('nombremodal','modalEdit')
        @slot('content')
            {!! Form::open(['url' => 'categories', 'method' => 'put', 'id' => 'edit-form']) !!}
                <div class="form-group">
                    <label for="name_edit">Nombre</label>
                    <input id="name_edit" name="name" type="text" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="status">Estatus</label>
                    {!!  Form::select('active', [0 => 'Inactivo',1 => 'Activo'], null, ['class' => 'form-control select2','id' => 'active_edit']) !!}
                </div>
        @endslot
        @slot('button_id')
            btn_update_type
        @endslot
        @slot('icon')
            <i class="fa fa-refresh"></i>
        @endslot
        @slot('title_action')
            Actualizar
            {!! Form::close() !!}
        @endslot
    @endcomponent
    {!! Form::open(['url' => '/categories', 'method' =>'delete', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}
@endsection
@section('extra-js')    
    <script src="{{asset('/js/categories.js')}}"></script>
@endsection