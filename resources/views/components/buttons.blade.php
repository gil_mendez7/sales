<div>
    <a class="btn btn-danger" href="{{$url}}">
        <i class="fa fa-times"></i>
        Cancelar
    </a>
    <button class="btn btn-primary" type="submit">
        {{$body_button}}
    </button>
</div>