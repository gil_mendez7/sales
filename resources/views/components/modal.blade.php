<div class="modal" id="{{$nombremodal ?? 'modal'}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{$title}}
                </h5>
                <div class="modal-body">
                    {{$content}}
                </div>
            </div>
            <div class="modal-footer">
                
                <a data-modal="{{$nombremodal ?? 'modal'}}" class="close-modal btn red">Cerrar</a>
                <button type="submit"  id="{{$button_id}}" class="btn">
                    {{$icon}}
                    {{$title_action}}
                </button>
            </div>
        </div>
    </div>
</div>